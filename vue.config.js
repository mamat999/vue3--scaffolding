const { defineConfig } = require('@vue/cli-service')

module.exports = defineConfig({
  devServer: {
    port:9191
  },
  transpileDependencies: true,
    chainWebpack: config =>{
    config.plugin('html')
        .tap(args => {
          args[0].title = "OJ管理员端系统管理设计与开发";
          return args;
        })
  }
})
