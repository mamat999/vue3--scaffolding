import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: '首页',
    component: HomeView
  },
  {
    path: '/about',
    name: '关于',
    component: () => import('../views/AboutView.vue')
  },
  {
    path: '/login',
    name: '登录',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/register',
    name: '注册',
    component: () => import('../views/Register.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

// currentPathName路由设置
router.beforeEach((to,from,next)=>{
  localStorage.setItem("currentPathName",to.name)
  // store.commit("setPath")
  next()
})
export default router
