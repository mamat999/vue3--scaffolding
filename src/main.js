import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './assets/global.css'
import ElementPlus from 'element-plus'
import * as ElementPlusIconVue from '@element-plus/icons-vue'
import 'element-plus/dist/index.css'
import request from './utils/request'
import store from './store'
import * as echarts from 'echarts';

const app= createApp(App)
for(const [key,component] of Object.entries(ElementPlusIconVue)){
    app.component(key,component)
}
app.use(ElementPlus,{size:"small"})
app.use(store)

app.use(router)
app.config.globalProperties.$echarts = echarts;
app.config.globalProperties.$axios=request;
app.mount('#app')
