import { createStore } from 'vuex'
// import router, { resetRouter } from '@/router';

export default createStore({
  state: {
    currentPathName: '',
  },
  mutations: {
    setPath(state){
      state.currentPathName=localStorage.getItem("currentPathName")
    },
  },
  getters: {
},
  actions: {
  },
  modules: {
  }
})
